<?php
header('Access-Control-Allow-Origin: *');

function getDescriptionById($id) {
    switch ($id) {
        case 1:
            return "El pitjor del mon :smile:";
        case 2:
            return "Maldito conejo";
        case 3:
            return "Vale oc :)";
    }
    return "";
}

function endsWith($haystack, $needle)
{
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

// Read all photos
$currentDir = realpath(dirname(__FILE__));
$files = scandir($currentDir);
$photos = array();
foreach ($files as $file) {
    if (endsWith($file, ".jpg")) {
        $exploded = explode('.', $file);
        $id = "";
        $name = "";
        if (sizeof($exploded) == 3) {
            $id = $exploded[0];
            $name = $exploded[1];
        }
        array_push($photos, array('img' => $file, 'caption' => $name, 'desc' => getDescriptionById($id)));
    }
}
//shuffle($photos);

echo json_encode($photos);
?>